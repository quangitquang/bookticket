<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    protected $roleModel;
    protected $userModel;

    public function __construct(User $user, Role $role)
    {
        $this->userModel = $user;
        $this->roleModel = $role;
    }

    // public function KTLogin(){
    //     $admin = Session::get('id');
    //     if($admin){
    //         return Redirect::to('/dashboard');
    //     }else{
    //         return Redirect::to('/admin')->send();
    //     }
    // }
    public function index(){
        return view('admin.dashboard.admin_login');
    }


    public function show_dashboard(){
        return view('admin.dashboard.index');
    }

    public function create()
    {   
        $role = Role::all();

        return view('admin.dashboard.add_user', compact('role'));
    }

    public function add(Request $request)
    {   
        $this->validation($request);

        $dataCreate = $request->all();
        $dataCreate['password'] = Hash::make($request->input('password'));
        $user = $this->userModel->create($dataCreate);

        $user->syncRoles($request->roles);

        return redirect::to('/all-user')->with('message','Thêm Thành Công');
    }

    public function validation(Request $request)
    {
        return $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:8',
            
        ],[
            'name.required' => 'tên người dùng không được để trống',
            'email.required' => 'địa chỉ email không được để trống',
            'email.email' => 'địa chỉ email không hợp lệ',
            'password.min' => 'mật khẩu phải nhiều hơn 8 ký tự',
            
        ]);
    }

    public function show()
    {
        $users = $this->userModel->with('roles')->latest('id')->paginate(10);

        return view('admin.dashboard.all_user')->with('users',$users);
    }

    public function edit($id)
    {
        $role = Role::all();
        $edit_user = DB::table('users')->where('id',$id)->get();
        $manager_user = view('admin.dashboard.edit_user',compact('role'))->with('edit_users',$edit_user);
        return view('admin.layout.admin_layout')->with('admin.dashboard.edit_user',$manager_user);
    }

    public function update(Request $request,$id)
    {
        $user = $this->userModel->findOrFail($id);
        $dataUpdate = $request->all();
        $user->update($dataUpdate);
        $user->syncRoles($request->roles);
        
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        return redirect::to('/all-user')->with('message','Cập Nhật Thành Công');
    }

    public function destroy($id)
    {
        $user = Session::get('id');
        if($user == $id){
            return redirect::to('/all-user')->with('message','Bạn Không Có Quyền Xóa Chính Mình');
        }else{
            DB::table('users')->where('id',$id)->delete();
            return redirect::to('/all-user')->with('message','Xóa Thành Công');
        }
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        
    }
}
