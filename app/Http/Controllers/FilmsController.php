<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Image;
use App\Models\Films;

class FilmsController extends Controller
{

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $cate_categories = DB::table('categories')->orderBy('category_id','desc')->get();
        
        return view('admin.dashboard.add_film')->with('cate_categories',$cate_categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array();
        $data['film_name'] = $request->film_name;
        $data['category_id'] = $request->film_cate;
        $data['film_time'] = $request->film_time;
        $data['film_date'] = $request->film_date;
        $data['film_content'] = $request->film_content;
        $data['film_director'] = $request->film_director;
        $data['film_country'] = $request->film_country;
        $data['film_trailer'] = $request->film_trailer;
        $data['film_actors'] = $request->film_actor;
        
        $get_image = $request->file('film_image');
        if($get_image){   
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));//explode dùng phân tách tên giữa 2 bên dấu chấm
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('upload/films',$new_image);
            $data['film_image'] =$new_image;
            DB::table('films')->insert($data);
            Session::put('message','Thêm Thành Công');
            return Redirect::to('all-film');
        }
        $data['film_image'] = '';
        DB::table('films')->insert($data);
        Session::put('message','Thêm Thành Công');
        return Redirect::to('all-film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $all_film = DB::table('films')
        ->join('categories','films.category_id','=','categories.category_id')
        ->select('films.*','categories.*')->get();
        // 'films.film_id','films.film_name','films.film_time','categories.category_name','films.film_date','films.film_image','films.film_content'
        $manager_category = view('admin.dashboard.all_film')->with('all_films',$all_film);
        return view('admin.layout.admin_layout')->with('admin.dashboard.all_film',$manager_category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($film_id)
    {
        $cate_categories = DB::table('categories')->orderBy('category_id','desc')->get();
        $edit_film = DB::table('films')->where('film_id',$film_id)->get();

        $manager_film = view('admin.dashboard.edit_film')->with('edit_films',$edit_film)->with('cate_categories',$cate_categories);
        return view('admin.layout.admin_layout')->with('admin.dashboard.edit_film',$manager_film);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $film_id)
    {
        $this->KTlogin();
        $data = array();
        $data['film_name'] = $request->film_name;
        $data['category_id'] = $request->film_cate;
        $data['film_time'] = $request->film_time;       
        $data['film_date'] = $request->film_date;
        $data['film_content'] = $request->film_content;
        $data['film_director'] = $request->film_director;
        $data['film_country'] = $request->film_country;
        $data['film_trailer'] = $request->film_trailer;
        $data['film_actors'] = $request->film_actor;
        $get_image = $request->file('film_image');
        if($get_image){
            
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));//explode dùng phân tách tên giữa 2 bên dấu chấm
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('upload/films',$new_image);
            $data['film_image'] =$new_image;
            DB::table('films')->where('film_id',$film_id)->update($data);
            Session::put('message','cập nhật Thành Công');
            return Redirect::to('all-film');
        }
        DB::table('films')->where('film_id',$film_id)->update($data);
        return redirect::to('/all-film')->with('message','Cập Nhật Thành Công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($film_id)
    {
        $this->KTlogin();
        DB::table('films')->where('film_id',$film_id)->delete();
        
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        return redirect::to('/all-film')->with('message','Xóa Thành Công');
    }

    //end funtion admin page

    public function show_category($category_id){
        $all_category = DB::table('categories')->orderBy('category_id','desc')->get();

        $cate_by_id = DB::table('categories')
        ->join('films','films.category_id','=','categories.category_id')
        ->select('categories.*','films.*')->where('films.category_id',$category_id)->orderBy('film_id','desc')->limit(6)->get();

        return view('pages.films.show_film')->with('all_categories',$all_category)->with('cate_by_id',$cate_by_id);
    }

    public function details_film($film_id){
        $all_category = DB::table('categories')->orderBy('category_id','desc')->get();

        $films_detail = DB::table('films')
        ->join('categories','films.category_id','=','categories.category_id')
        ->select('films.*','categories.*')->where('films.film_id',$film_id)->get();

        return view('pages.films.film_detail')->with('all_categories',$all_category)->with('film_detail',$films_detail);
    }


}
