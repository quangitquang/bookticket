<?php

namespace App\Http\Controllers;

use App\Models\Cinema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class CinemaController extends Controller
{
    public function KTLogin(){
        $admin = Session::get('admin_id');
        if($admin){
            return Redirect::to('/dashboard');
        }else{
            return Redirect::to('/admin')->send();
        }
    }
    
    public function index()
    {
        //
    }

    
    public function create()
    {
        $this->KTLogin();
        return view('admin.dashboard.add_cinema');
    }

    
    public function store(Request $request)
    {
        $this->KTLogin();
        // $Ticket = new Tickets;
        // $Ticket->category_name = $request->categories_name;
        // $Ticket->category_status = $request->categories_status;
        // $Categories->save();
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        $data = $request ->all();
        $ticket = new Cinema();
        $ticket->cinema_name = $data['cinema_name'];
        $ticket->cinema_address = $data['cinema_address'];
        $ticket->save();
        return redirect::to('/all-cinema')->with('message','Thêm Thành Công');
    }

    
    public function show()
    {
        $this->KTLogin();
        //$all_category = DB::table('categories')->get();
        //$all_ticket = Tickets::orderby('tickets_id','desc')->paginate(số lượng trong 1 trang);
        $all_cinema = Cinema::all();
        $manager_cinema = view('admin.dashboard.all_cinema')->with('all_cinema',$all_cinema);
        return view('admin.layout.admin_layout')->with('admin.dashboard.all_cinema',$manager_cinema);
    }

   
    public function edit($cinema_id)
    {
        $this->KTLogin();
        $edit_cinema = Cinema::where('cinema_id',$cinema_id)->get();
        //$edit_cinema = DB::table('cinema')->where('cinema_id',$cinema_id)->get();
        $manager_cinema = view('admin.dashboard.edit_cinema')->with('edit_cinema',$edit_cinema);
        return view('admin.layout.admin_layout')->with('admin.dashboard.edit_cinema',$manager_cinema);
    }

    
    public function update(Request $request, $cinema_id)
    {
        $this->KTLogin();
        $cinema = array();
        $cinema['cinema_name']=$request->cinema_name;
        $cinema['cinema_address'] = $request->cinema_address;
        Cinema::where('cinema_id',$cinema_id)->update($cinema);
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        return redirect::to('/all-cinema')->with('message','Cập Nhật Thành Công');
    }

    
    public function destroy($cinema_id)
    {
        $this->KTLogin();
        Cinema::where('cinema_id',$cinema_id)->delete();
        
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        return redirect::to('/all-cinema')->with('message','Xóa Thành Công');
    }
}
