<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Image;
use App\Models\Films;
use App\User;
use Illuminate\Support\Facades\Auth;

class CheckOutController extends Controller
{
    public function login_chekout(){
        $all_category = DB::table('categories')->orderBy('category_id','desc')->get();

        return view('pages.films.login_checkout')->with('all_categories',$all_category);
    }

    public function add_customer(Request $request){

        $data = array();
        $data['customer_name'] = $request->name_account;
        $data['customer_email'] = $request->email_account;
        $data['customer_password'] = md5($request->password_account);
        $data['customer_phone'] = $request->phone_account;

        $customer_id = DB::table('customers')->insertGetId($data);
        Session::put('customer_id',$customer_id);
        Session::put('customer_name',$request->name_account);
        return Redirect::to('/checkout');
    }

    public function checkout(){
        // $all_category = DB::table('categories')->orderBy('category_id','desc')->get();
        $all_booking = DB::table('bookings')
        ->join('tickets','tickets.tickets_id','=','bookings.ticket_id')
        ->join('films','films.film_id','=','bookings.film_id')
        ->select('films.*','tickets.ticket_money','tickets.ticket_name','bookings.*')
        ->get();
        //insert oder
        $oder_ticket = array();
        $oder_ticket['customer_id'] = Session::get('customer_id');
        $oder_id = DB::table('oders')->insertGetId($oder_ticket);
        //dd($oder_ticket);
        //insert oder_detail
        foreach($all_booking as $boking)
        {
            $oderD_ticket['oder_id']=$oder_id;
            $oderD_ticket['film_id']=$boking->film_id;
            $oderD_ticket['ticket_id']=$boking->ticket_id;
            $oderD_ticket['filmname']=$boking->film_name;
            $oderD_ticket['ticket_name']=$boking->ticket_name;
            $oderD_ticket['date']=$boking->booking_date;
            $oderD_ticket['Time']=$boking->booking_time;
            $oderD_ticket['money']=$boking->ticket_money;
            $oderD_ticket['quantity']=$boking->quantity;    
            $oderD_ticket['total']=($boking->ticket_money)*($boking->quantity);    
            DB::table('oder_detail')->insert($oderD_ticket);
            // dd($oderD_ticket);
        }
        DB::table('bookings')->delete();
        return redirect('/show-ticket/{customer_id}');
        
    }

    public function logout_chekout(){
        Session::flush();
        return redirect('/');
    }

    public function login_customer(Request $request){
        // $email = $request->email_account;
        // $password =md5($request->password_account);

        // $result = DB::table('customers')->where('customer_email',$email)->where('customer_password',$password)->first();
        // if($result){
        //     Session::put('customer_id',$result->customer_id);  
        //     return redirect::to('/');
        // }return redirect::to('/login-checkout');   
        if(Auth::attempt(['email'=>$request->email, 'password'=> $request->password ]))
        {
            
            return redirect::to('/');
        }else{
            return redirect::to('/login-checkout')->with('message','login failed');
        }

// auth()->check() -> check dang nhap . auth()->user() -> lasy thong tin

    }

    public function show($customer_id)
    {   
        $customer_id=Session::get('customer_id');
        $all_category = DB::table('categories')->orderBy('category_id','desc')->get();
        $all_booking = DB::table('oders')
        ->join('customers','customers.customer_id','=','oders.customer_id')
        ->join('oder_detail','oder_detail.oder_id','=','oders.oder_id')
        ->select('oders.*','oder_detail.*')->where('customers.customer_id',$customer_id)->orderBy('oders.oder_id','desc')->get();
        return view('pages.checkout.show_ticket')->with('all_booking',$all_booking)->with('all_categories',$all_category);
    }
}
