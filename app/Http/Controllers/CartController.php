<?php

namespace App\Http\Controllers;

use App\Models\Tickets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Image;
use Gloudemans\Shoppingcart\Cart;

class CartController extends Controller
{
    

    public function book($film_id)
    {
        $all_category = DB::table('categories')->orderBy('category_id','desc')->get();
        $data = DB::table('films')->where('film_id',$film_id)->get();
        $all_ticket = Tickets::all();
        return view('pages.films.book_ticket')->with('booking',$data)->with('all_categories',$all_category)->with('all_tickets',$all_ticket);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $all_category = DB::table('categories')->orderBy('category_id','desc')->get();
        $all_ticket = Tickets::all();

        return view('pages.films.show_booking')->with('all_categories',$all_category)->with('all_tickets',$all_ticket);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
