<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        
        return view('admin.dashboard.add_categories');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $Categories = new Categories;
        $Categories->category_name = $request->categories_name;
        $Categories->category_status = $request->categories_status;
        $Categories->save();
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        return redirect::to('/Category/all-category')->with('message','Thêm Thành Công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
        $all_category = DB::table('categories')->get();
        $manager_category = view('admin.dashboard.all_categories')->with('all_categories',$all_category);
        return view('admin.layout.admin_layout')->with('admin.dashboard.all_categories',$manager_category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($category_id)
    {
        
        $edit_category = DB::table('categories')->where('category_id',$category_id)->get();

        
        $manager_category = view('admin.dashboard.edit_categories')->with('edit_categories',$edit_category);
        return view('admin.layout.admin_layout')->with('admin.dashboard.edit_categories',$manager_category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$category_id)
    {
        
        $Categories = array();
        $Categories['category_name'] = $request->categories_name;
        $Categories['category_status'] = $request->categories_status;
        DB::table('categories')->where('category_id',$category_id)->update($Categories);
        
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        return redirect::to('/Category/all-category')->with('message','Cập Nhật Thành Công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($category_id)
    {
        
        DB::table('categories')->where('category_id',$category_id)->delete();
        
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        return redirect::to('/Category/all-category')->with('message','Xóa Thành Công');
    }
}
