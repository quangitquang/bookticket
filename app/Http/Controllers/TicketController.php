<?php

namespace App\Http\Controllers;

use App\Models\Tickets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class TicketController extends Controller
{
    
    public function index()
    {
        //
    }

    
    public function create()
    {
        return view('admin.dashboard.add_ticket');
    }

    
    public function store(Request $request)
    {
        // $Ticket = new Tickets;
        // $Ticket->category_name = $request->categories_name;
        // $Ticket->category_status = $request->categories_status;
        // $Categories->save();
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        $data = $request ->all();
        $ticket = new Tickets();
        $ticket->ticket_name = $data['tickets_name'];
        $ticket->ticket_money = $data['tickets_money'];
        $ticket->save();
        return redirect::to('/all-ticket')->with('message','Thêm Thành Công');
    }

    
    public function show()
    {
        //$all_category = DB::table('categories')->get();
        //$all_ticket = Tickets::orderby('tickets_id','desc')->paginate(số lượng trong 1 trang);
        $all_ticket = Tickets::all();
        $manager_ticket = view('admin.dashboard.all_ticket')->with('all_tickets',$all_ticket);
        return view('admin.layout.admin_layout')->with('admin.dashboard.all_ticket',$manager_ticket);
    }

   
    public function edit($tickets_id)
    {
        $edit_ticket = Tickets::where('tickets_id',$tickets_id)->get();
        //$edit_ticket = DB::table('tickets')->where('tickets_id',$tickets_id)->get();
        $manager_tickets = view('admin.dashboard.edit_ticket')->with('edit_ticket',$edit_ticket);
        return view('admin.layout.admin_layout')->with('admin.dashboard.edit_ticket',$manager_tickets);
    }

    
    public function update(Request $request, $tickets_id)
    {
        $ticket = array();
        $ticket['ticket_name']=$request->ticket_name;
        $ticket['ticket_money'] = $request->ticket_money;
        Tickets::where('tickets_id',$tickets_id)->update($ticket);
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        return redirect::to('/all-ticket')->with('message','Cập Nhật Thành Công');
    }

    
    public function destroy($tickets_id)
    {
        Tickets::where('tickets_id',$tickets_id)->delete();
        
        // Session::put('message','Thêm Thành Công :))');
        // return Redirect::to('categories/create');
        return redirect::to('/all-ticket')->with('message','Xóa Thành Công');
    }
}
