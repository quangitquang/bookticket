<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register_auth(){
        $role = Role::all();
        return view('admin.custom_auth.add_user',compact('role'));
    }
    
    

    public function login_auth(){
        return view('admin.custom_auth.login_auth');
    }


}
