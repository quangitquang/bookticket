<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Films;
use App\Models\Tickets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class BookingController extends Controller
{
    public function save_booking(Request $request)
    {
        $data = array();
        $data['film_id'] = $request->film_id;
        $data['ticket_id'] = $request->book_tick;
        $data['booking_date'] = $request->booking_date;
        $data['booking_time'] = $request->booking_time;
        $data['quantity'] = $request->quantity;
        DB::table('bookings')->insert($data);
        return redirect::to('/booking');
    }
    public function show()
    {   $all_category = DB::table('categories')->orderBy('category_id','desc')->get();
        $all_booking = DB::table('bookings')
        ->join('tickets','tickets.tickets_id','=','bookings.ticket_id')
        ->join('films','films.film_id','=','bookings.film_id')
        ->select('films.*','tickets.*','bookings.*')
        ->get();
        return view('pages.films.show_booking')->with('all_booking',$all_booking)->with('all_categories',$all_category);
    }
}
