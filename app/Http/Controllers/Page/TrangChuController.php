<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class TrangChuController extends Controller
{
    public function index(){

        $all_category = DB::table('categories')->orderBy('category_id','desc')->get();

        $all_films = DB::table('films')
        ->join('categories','films.category_id','=','categories.category_id')
        ->select('films.*','categories.*')->orderBy('film_id','desc')->limit(6)->get();

        return view('pages.content')->with('all_films',$all_films)->with('all_categories',$all_category);
    }
}
