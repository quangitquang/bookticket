<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Films extends Model
{
    public $timestamps = false;
    protected $fillable = [
            'film_name',
            'film_country',
            'film_director',
            'film_actors',
            'film_trailer',
            'film_time',
            'film_date',
            'film_image',
           'film_content'
    ];
    protected $primarykey = 'film_id';
    protected $table = 'films';
}
