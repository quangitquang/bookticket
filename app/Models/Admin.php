<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model ;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Middleware\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class Admin extends Model
{
    use Notifiable;
    use HasRoles;

    public $timestamps = false;
    protected $fillable = [
        'admin_password','admin_email','admin_phone','admin_name'
    ];
    protected $primarykey = 'admin_id';
    protected $table = 'admin_login';
    
    // public function admin(){
    //     return $this->belongsToMany('App\Models\Role');
    // }

    // public function getAuthPassword(){
    //     return $this->admin_password;
    // }
}
