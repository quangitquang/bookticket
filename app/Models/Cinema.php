<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'cinema_name','cinema_address'
    ];
    protected $primarykey = 'cinema_id';
    protected $table = 'cinemas';
}
