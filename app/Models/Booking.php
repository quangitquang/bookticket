<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public $timestamps = false;
    protected $fillable = [ 
        'film_id',
        'ticket_id',
        'booking_date',
        'booking_time',
        'price'
    ];
    protected $primarykey = 'booking_id';
    protected $table = 'bookings';
}
