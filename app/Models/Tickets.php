<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'ticket_name','category_money'
    ];
    protected $primarykey = 'ticket_id';
    protected $table = 'tickets';
    // public function lichchieu(){
    //     return $this->hasMany('App\lichchieu','ticket_id');
    // } -> có nghĩa là theo cái bảng lịch chiếu lấy đến ticket_id -> 1 lịch chiều có nhiều id
}
