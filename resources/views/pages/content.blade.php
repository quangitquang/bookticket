@extends('welcome')
@section('content')
<section class="container">
    <div class="movie-best">
        <div class="col-sm-10 col-sm-offset-1 movie-best__rating">Hôm Nay Có Gì</div>
        <div class="col-sm-12 change--col">
        @foreach($all_films as $key => $film_pro)
            <div class="movie-beta__item ">
                <img alt='' src="{{asset('upload/films/'.$film_pro->film_image)}}" height="300">
                <span class="best-rate">5.0</span>

                <ul class="movie-beta__info">
                    <li><span class="best-voted">71 people voted today</span></li>
                    <li>
                        <p class="movie__time">{{$film_pro->film_time}}</p>
                        <p>{{$film_pro->category_name}} </p>
                        <p>38 comments</p>
                    </li>
                    <li class="last-block">
                        <a href="{{URL::to('/film-detail/'.$film_pro->film_id)}}" class="slide__link">more</a>
                    </li>
                </ul>
            </div>
        @endforeach   
        </div>
        <div class="col-sm-10 col-sm-offset-1 movie-best__check">check all movies now playing</div>
    </div>

    <div class="col-sm-12">
        <div class="mega-select-present mega-select-top mega-select--full">
            <div class="mega-select-marker">
                <div class="marker-indecator location">
                    <p class="select-marker"><span>movie to watch now</span> <br>in your city</p>
                </div>

                <div class="marker-indecator cinema">
                    <p class="select-marker"><span>find your </span> <br>cinema</p>
                </div>

                <div class="marker-indecator film-category">
                    <p class="select-marker"><span>find movie due to </span> <br> your mood</p>
                </div>

                <div class="marker-indecator actors">
                    <p class="select-marker"><span> like particular stars</span> <br>find them</p>
                </div>

                <div class="marker-indecator director">
                    <p class="select-marker"><span>admire personalities - find </span> <br>by director</p>
                </div>

                <div class="marker-indecator country">
                    <p class="select-marker"><span>search for movie from certain </span> <br>country?</p>
                </div>
            </div>

            <div class="mega-select pull-right">
                <span class="mega-select__point">Search by</span>
                <ul class="mega-select__sort">
                    <li class="filter-wrap"><a href="#" class="mega-select__filter filter--active" data-filter='location'>Location</a></li>
                    <li class="filter-wrap"><a href="#" class="mega-select__filter" data-filter='cinema'>Cinema</a></li>
                    <li class="filter-wrap"><a href="#" class="mega-select__filter" data-filter='film-category'>Category</a></li>
                    <li class="filter-wrap"><a href="#" class="mega-select__filter" data-filter='actors'>Actors</a></li>
                    <li class="filter-wrap"><a href="#" class="mega-select__filter" data-filter='director'>Director</a></li>
                    <li class="filter-wrap"><a href="#" class="mega-select__filter" data-filter='country'>Country</a></li>
                </ul>

                <input name="search-input" type='text' class="select__field">

                <div class="select__btn">
                    <a href="#" class="btn btn-md btn--danger location">find <span class="hidden-exrtasm">your city</span></a>
                    <a href="#" class="btn btn-md btn--danger cinema">find <span class="hidden-exrtasm">suitable cimema</span></a>
                    <a href="#" class="btn btn-md btn--danger film-category">find <span class="hidden-exrtasm">best category</span></a>
                    <a href="#" class="btn btn-md btn--danger actors">find <span class="hidden-exrtasm">talented actors</span></a>
                    <a href="#" class="btn btn-md btn--danger director">find <span class="hidden-exrtasm">favorite director</span></a>
                    <a href="#" class="btn btn-md btn--danger country">find <span class="hidden-exrtasm">produced country</span></a>
                </div>

                <div class="select__dropdowns">
                    <ul class="select__group location">
                        <li class="select__variant" data-value='London'>London</li>
                        <li class="select__variant" data-value='New York'>New York</li>
                        <li class="select__variant" data-value='Paris'>Paris</li>
                        <li class="select__variant" data-value='Berlin'>Berlin</li>
                        <li class="select__variant" data-value='Moscow'>Moscow</li>
                        <li class="select__variant" data-value='Minsk'>Minsk</li>
                        <li class="select__variant" data-value='Warsawa'>Warsawa</li>
                    </ul>

                    <ul class="select__group cinema">
                        <li class="select__variant" data-value='Cineworld'>Cineworld</li>
                        <li class="select__variant" data-value='Empire'>Empire</li>
                        <li class="select__variant" data-value='Everyman'>Everyman</li>
                        <li class="select__variant" data-value='Odeon'>Odeon</li>
                        <li class="select__variant" data-value='Picturehouse'>Picturehouse</li>
                    </ul>

                    <ul class="select__group film-category">
                        <li class="select__variant" data-value="Children's">Children's</li>
                        <li class="select__variant" data-value='Comedy'>Comedy</li>
                        <li class="select__variant" data-value='Drama'>Drama</li>
                        <li class="select__variant" data-value='Fantasy'>Fantasy</li>
                        <li class="select__variant" data-value='Horror'>Horror</li>
                        <li class="select__variant" data-value='Thriller'>Thriller</li>
                    </ul>

                    <ul class="select__group actors">
                        <li class="select__variant" data-value='Leonardo DiCaprio'>Leonardo DiCaprio</li>
                        <li class="select__variant" data-value='Johnny Depp'>Johnny Depp</li>
                        <li class="select__variant" data-value='Jack Nicholson'>Jack Nicholson</li>
                        <li class="select__variant" data-value='Robert De Niro'>Robert De Niro</li>
                        <li class="select__variant" data-value='Morgan Freeman'>Morgan Freeman</li>
                        <li class="select__variant" data-value='Jim Carrey'>Jim Carrey</li>
                        <li class="select__variant" data-value='Adam Sandler'>Adam Sandler</li>
                        <li class="select__variant" data-value='Ben Stiller'>Ben Stiller</li>
                    </ul>

                    <ul class="select__group director">
                        <li class="select__variant" data-value='Steven Spielberg'>Steven Spielberg</li>
                        <li class="select__variant" data-value='Martin Scorsese'>Martin Scorsese</li>
                        <li class="select__variant" data-value='Guy Ritchie'>Guy Ritchie</li>
                        <li class="select__variant" data-value='Christopher Nolan'>Christopher Nolan</li>
                        <li class="select__variant" data-value='Tim Burton'>Tim Burton</li>
                    </ul>

                    <ul class="select__group country">
                        <li class="select__variant" data-value='USA'>USA</li>
                        <li class="select__variant" data-value='Germany'>Germany</li>
                        <li class="select__variant" data-value='Australia'>Australia</li>
                        <li class="select__variant" data-value='UK'>UK</li>
                        <li class="select__variant" data-value='Japan'>Japan</li>
                        <li class="select__variant" data-value='Serbia'>Serbia</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <h2 id='target' class="page-heading heading--outcontainer">Đang Chiếu Ở Rạp</h2>

    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-8 col-md-9">
                <!-- Movie variant with time -->
                @foreach($all_films as $key => $film_pro)

                <div class="movie movie--test movie--test--dark movie--test--left">
                    <div class="movie__images">
                        <a href="{{URL::to('/film-detail/'.$film_pro->film_id)}}" class="movie-beta__link">
                            <img alt="" src="{{asset('upload/films/'.$film_pro->film_image)}}" width="300">
                        </a>
                    </div>

                    <div class="movie__info">
                        <a href="{{URL::to('/film-detail/'.$film_pro->film_id)}}" class="movie__title">{{$film_pro->film_name}}</a>

                        <p class="movie__time">{{$film_pro->film_time}}</p>
                        
                        <p class="movie__option"><a href="#">{{$film_pro->category_name}}</a></p>
                        <p class="movie__option"><a href="#">{{$film_pro->film_date}}</a></p>
                        <div class="movie__rate">
                            <div class="score"></div>
                            <span class="movie__rating">9.0</span>
                        </div>
                    </div>
                </div>
                
                @endforeach
                <!-- Movie variant with time -->

                
                <!-- Movie variant with time -->
            </div>

            <aside class="col-sm-4 col-md-3">
                <div class="sitebar first-banner--left">
                @foreach($all_films as $key => $film_pro)
                    <div class="banner-wrap first-banner--left">
                        <img alt='banner' src="{{asset('upload/films/'.$film_pro->film_image)}}">
                    </div>
                @endforeach
                </div>
            </aside>
        </div>
    </div>

    <div class="col-sm-12">
        <h2 class="page-heading">Latest news</h2>

        <div class="col-sm-4 similar-wrap col--remove">
            <div class="post post--preview post--preview--wide">
                <div class="post__image">
                    <img alt='' src="{{asset('upload/films/mortal44.jpg')}}" width="270">
                    <div class="social social--position social--hide">
                        <span class="social__name">Share:</span>
                        <a href='#' class="social__variant social--first fa fa-facebook"></a>
                        <a href='#' class="social__variant social--second fa fa-twitter"></a>
                        <a href='#' class="social__variant social--third fa fa-vk"></a>
                    </div>
                </div>
                <p class="post__date">16-04-2021 </p>
                <a href="single-page-left.html" class="post__title">Review phim Cuộc chiến sinh tử - phim hành động “đã tai đã mắt” tuần này</a>
                <a href="single-page-left.html" class="btn read-more post--btn">read more</a>
            </div>
        </div>
        <div class="col-sm-4 similar-wrap col--remove">
            <div class="post post--preview post--preview--wide">
                <div class="post__image">
                    <img alt='' src="{{asset('upload/films/'.$film_pro->film_image)}}">
                    <div class="social social--position social--hide">
                        <span class="social__name">Share:</span>
                        <a href='#' class="social__variant social--first fa fa-facebook"></a>
                        <a href='#' class="social__variant social--second fa fa-twitter"></a>
                        <a href='#' class="social__variant social--third fa fa-vk"></a>
                    </div>
                </div>
                <p class="post__date">12-03-2021 </p>
                <a href="single-page-left.html" class="post__title">Top 10 phim Việt Nam nên xem trên Netflix</a>
                <a href="single-page-left.html" class="btn read-more post--btn">read more</a>
            </div>
        </div>
        <div class="col-sm-4 similar-wrap col--remove">
            <div class="post post--preview post--preview--wide">
                <div class="post__image">
                    <img alt='' src="{{asset('upload/films/mortal44.jpg')}}">
                    <div class="social social--position social--hide">
                        <span class="social__name">Share:</span>
                        <a href='#' class="social__variant social--first fa fa-facebook"></a>
                        <a href='#' class="social__variant social--second fa fa-twitter"></a>
                        <a href='#' class="social__variant social--third fa fa-vk"></a>
                    </div>
                </div>
                <p class="post__date">14-04-2021 </p>
                <a href="single-page-left.html" class="post__title">Top 10 phim hành động phê tận óc trên Netflix</a>
                <a href="single-page-left.html" class="btn read-more post--btn">read more</a>
            </div>
        </div>
    </div>

</section>
@endsection