<header class="header-wrapper header-wrapper--home">
    <div class="container">
        <!-- Logo link-->
        <a href='index.html' class="logo">
            <img alt='logo' src="images/logo.png">
        </a>

        <!-- Main website navigation-->
        <nav id="navigation-box">
            <!-- Toggle for mobile menu mode -->
            <a href="{{url('/')}}" id="navigation-toggle">
                <span class="menu-icon">
                    <span class="icon-toggle" role="button" aria-label="Toggle Navigation">
                        <span class="lines"></span>
                    </span>
                </span>
            </a>

            <!-- Link navigation -->
            <ul id="navigation">
                <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="{{url('/')}}">Trang chủ</a>
                </li>
                <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="#">Thể Loại</a>
                    <ul>
                        @foreach($all_categories as $key => $cate_pro)
                        <li class="menu__nav-item"><a href="{{URL::to('/danh-muc-phim/'.$cate_pro->category_id)}}">{{$cate_pro->category_name}}</a></li>
                        @endforeach
                    </ul>
                </li>

                <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="news-left.html">Tin Tức</a>
                    <ul>
                        <li class="menu__nav-item"><a href="news-left.html">News (rigth sidebar)</a></li>
                        <li class="menu__nav-item"><a href="news-right.html">News (left sidebar)</a></li>
                        <li class="menu__nav-item"><a href="news-full.html">News (full widht)</a></li>
                        <li class="menu__nav-item"><a href="single-page-left.html">Single post (rigth sidebar)</a></li>
                        <li class="menu__nav-item"><a href="single-page-right.html">Single post (left sidebar)</a></li>
                        <li class="menu__nav-item"><a href="single-page-full.html">Single post (full widht)</a></li>
                    </ul>
                </li>
                <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="#">Danh Sách Phim</a>
                    <ul class="mega-menu container">
                        <li class="col-md-3 mega-menu__coloum">
                            <h4 class="mega-menu__heading">Đang Chiếu</h4>
                            <ul class="mega-menu__list">
                                <li class="mega-menu__nav-item"><a href="#">The Counselor</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Bad Grandpa</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Blue Is the Warmest Color</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Capital</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Spinning Plates</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Bastards</a></li>
                            </ul>
                        </li>

                        <li class="col-md-3 mega-menu__coloum mega-menu__coloum--outheading">
                            <ul class="mega-menu__list">
                                <li class="mega-menu__nav-item"><a href="#">Gravity</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Captain Phillips</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Carrie</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Cloudy with a Chance of Meatballs 2</a></li>
                            </ul>
                        </li>

                        <li class="col-md-3 mega-menu__coloum">
                            <h4 class="mega-menu__heading">Sắp Chiếu</h4>
                            <ul class="mega-menu__list">
                                <li class="mega-menu__nav-item"><a href="#">Escape Plan</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Rush</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Prisoners</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Enough Said</a></li>
                                <li class="mega-menu__nav-item"><a href="#">The Fifth Estate</a></li>
                                <li class="mega-menu__nav-item"><a href="#">Runner Runner</a></li>
                            </ul>
                        </li>

                        <li class="col-md-3 mega-menu__coloum mega-menu__coloum--outheading">
                            <ul class="mega-menu__list">
                                <li class="mega-menu__nav-item"><a href="#">Insidious: Chapter 2</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>

        <!-- Additional header buttons / Auth and direct link to booking-->
        <!-- <div class="control-panel">
            <div class="auth auth--home">
                <div class="auth__show">
                    <span class="auth__image">
                        <img alt="" src="http://placehold.it/31x31">
                    </span>
                </div>
                <a href="#" class="btn btn--sign btn--singin login-window">
                Sign in
                </a>
                <ul class="auth__function">
                    <li><a href="#" class="auth__function-item">Watchlist</a></li>
                    <li><a href="#" class="auth__function-item">Booked tickets</a></li>
                    <li><a href="#" class="auth__function-item">Discussion</a></li>
                    <li><a href="#" class="auth__function-item">Settings</a></li>
                </ul>

            </div>
            <a href="#" class="btn btn-md btn--warning btn--book btn-control--home login-window">Book a ticket</a>
        </div> -->
        <div class="control-panel">
            <?php
            use Illuminate\Support\Facades\Auth;
            
            if (!Auth::id()) {
            ?>
                <a href="{{URL::to('/login-checkout')}}" class="btn btn--sign">Đăng Nhập</a>
            <?php
            } else {
            ?>
                <a href="{{URL::to('/logout-checkout')}}" class="btn btn--sign">Đăng Xuất</a>
            <?php
            }
            ?>
            <?php
            if (!Auth::id()) {
            ?>
                <a href="#" class="btn btn-md btn--warning btn--book login-window">Book a ticket</a>
            <?php
            } else {
            ?>
                <a href="{{URL::to('/show-ticket/{id}')}}" class="btn btn-md btn--warning btn--book">Book a ticket</a>
            <?php
            }
            ?>

        </div>

    </div>
</header>