<!-- Fonts -->
<!-- Font awesome - icon font -->
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<!-- Roboto -->
<link href="http://fonts.googleapis.com/css?family=Roboto:400,100,700" rel='stylesheet' type='text/css'>
<!-- Open Sans -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:800italic" rel='stylesheet' type='text/css'>

<!-- Stylesheets -->

<!-- Mobile menu -->
<link href="{{asset('frontend/css/gozha-nav.css')}}" rel="stylesheet" />
<!-- Select -->
<link href="{{asset('frontend/css/external/jquery.selectbox.css')}}" rel="stylesheet" />

<!-- REVOLUTION BANNER CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="{{asset('frontend/rs-plugin/css/settings.css')}}" media="screen" />

<!-- Custom -->
<link href="{{asset('frontend/css/style.css')}}?v=1" rel="stylesheet" />


<!-- Modernizr -->
<script src="{{asset('frontend/js/external/modernizr.custom.js')}}"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- [if lt IE 9]>  -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
<!-- <![endif] -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>