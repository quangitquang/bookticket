@extends('welcome')
@section('content')
<div class="search-wrapper">
    <div class="container container--add">
        <form id='search-form' method='get' class="search">
            <input type="text" class="search__field" placeholder="Search">
            <select name="sorting_item" id="search-sort" class="search__sort" tabindex="0">
                <option value="1" selected='selected'>By title</option>
                <option value="2">By year</option>
                <option value="3">By producer</option>
                <option value="4">By title</option>
                <option value="5">By year</option>
            </select>
            <button type='submit' class="btn btn-md btn--danger search__button">search a movie</button>
        </form>
    </div>
</div>

<!-- Main content -->

<section class="container">
    <div class="order-container">
        <div class="order">
            <img class="order__images" alt='' src="{{asset('frontend/images/tickets.png')}}">
            <p class="order__title">Đặt vé <br><span class="order__descript">and have fun movie time</span></p>
        </div>
    </div>
    <div class="choose-film">
        <div class="panel panel-default">
            <div class="panel-body">
                <form id="signupForm" action="{{URL::to('/next-booking')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <div class="col-md-4">
                            <h2 class="page-heading heading--outcontainer">Chọn Phim</h2>
                            <select name="film_id" class="form-control h-3" id="sel1">
                                @foreach($booking as $key => $film)
                                <option value="{{$film->film_id}}">{{$film->film_name}}</option>
                                @endforeach
                            </select>    
                        </div>

                        <div class="col-md-4">
                            <h2 class="page-heading heading--outcontainer">Chọn ngày</h2>
                            <div class="form-group">
                                <div id="filterDate2">
                                    <!-- Datepicker as text field -->
                                    <div class="input-group date" data-date-format="dd.mm.yyyy">
                                        <input name="booking_date" type="date" class="form-control" placeholder="dd.mm.yyyy">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h2 class="page-heading heading--outcontainer">Chọn xuất</h2>
                            <select name="booking_time" class="form-control" id="sel1">
                                <option>19:30</option>
                                <option>20:00</option>
                                <option>20:30</option>
                                <option>21:00</option>
                            </select>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <h2 class="page-heading heading--outcontainer">Chọn loại vé</h2>
                            <select name="book_tick" class="form-control" id="sel1">
                                @foreach($all_tickets as $key => $ticket)
                                <option value="{{$ticket->tickets_id}}">{{$ticket->ticket_name}}: {{number_format((float)$ticket->ticket_money).'VNĐ'}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <h2 class="page-heading heading--outcontainer">Số Lượng</h2>
                            <input class="form-control" type="number" id="quantity" name="quantity" min="1">
                        </div>
                    </div>
                    <div class="form-footer">
                        <!-- <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button> -->
                        <button type="submit" class="btn btn-md btn--shine">Đặt Vé</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection