@extends('welcome')
@section('content')
<!-- Search bar -->
<div class="search-wrapper">
    <div class="container container--add">
        <form id='search-form' method='get' class="search">
            <input type="text" class="search__field" placeholder="Search">
            <select name="sorting_item" id="search-sort" class="search__sort" tabindex="0">
                <option value="1" selected='selected'>By title</option>
                <option value="2">By year</option>
                <option value="3">By producer</option>
                <option value="4">By title</option>
                <option value="5">By year</option>
            </select>
            <button type='submit' class="btn btn-md btn--danger search__button">search a movie</button>
        </form>
    </div>
</div>
<section class="container">
    <div class="col-sm-12">
        <h2 class="page-heading">Movies</h2>

        <!-- Movie preview item -->
        @foreach($cate_by_id as $key => $film)
        <div class="movie movie--preview movie--full release">
            <div class="col-sm-3 col-md-2 col-lg-2">
                <div class="movie__images">
                    <img alt='' src="{{asset('upload/films/'.$film->film_image)}}">
                </div>
                <div class="movie__feature">
                    <a href="#" class="movie__feature-item movie__feature--comment">123</a>
                    <a href="#" class="movie__feature-item movie__feature--video">7</a>
                    <a href="#" class="movie__feature-item movie__feature--photo">352</a>
                </div>
            </div>

            <div class="col-sm-9 col-md-10 col-lg-10 movie__about">
                <a href="{{URL::to('/film-detail/'.$film->film_id)}}" class="movie__title link--huge">{{$film->film_name}}</a>

                <p class="movie__time">105 min</p>

                <p class="movie__option"><strong>Quốc Gia: </strong><a href="#">Việt Nam</a></p>
                <p class="movie__option"><strong>Thể Loại: </strong><a href="#">{{$film->category_name}} </a></p>
                <p class="movie__option"><strong>Ngày Chiếu: </strong>{{$film->film_date}}</p>
                
                <p class="movie__option"><strong>Age restriction: </strong><a href="#">13</a></p>

                <div class="movie__btns">
                    <a href="" class="btn btn-md btn-danger" data-toggle="modal" data-target="#trailer">Trailer</a>
                    <a href="{{URL::to('/book-ticket/'.$film->film_id)}}" class="watchlist btn--warning">Đặt Vé Bộ Phim Này</a>
                </div>

                <div class="preview-footer">
                    <div class="movie__rate">
                        <div class="score"></div><span class="movie__rate-number">170 votes</span> <span class="movie__rating">5.0</span>
                    </div>


                    <a href="#" class="movie__show-btn">Showtime</a>
                </div>
            </div>

            <div class="clearfix"></div>

            <!-- Time table (choose film start time)-->
            <!-- <div class="time-select">
                <div class="time-select__group group--first">
                    <div class="col-sm-4">
                        <p class="time-select__place">Cineworld</p>
                    </div>
                    <ul class="col-sm-8 items-wrap">
                        <li class="time-select__item" data-time='09:40'>09:40</li>
                        <li class="time-select__item" data-time='13:45'>13:45</li>
                        <li class="time-select__item active" data-time='15:45'>15:45</li>
                        <li class="time-select__item" data-time='19:50'>19:50</li>
                        <li class="time-select__item" data-time='21:50'>21:50</li>
                    </ul>
                </div>

                <div class="time-select__group">
                    <div class="col-sm-4">
                        <p class="time-select__place">Empire</p>
                    </div>
                    <ul class="col-sm-8 items-wrap">
                        <li class="time-select__item" data-time='10:45'>10:45</li>
                        <li class="time-select__item" data-time='16:00'>16:00</li>
                        <li class="time-select__item" data-time='19:00'>19:00</li>
                        <li class="time-select__item" data-time='21:15'>21:15</li>
                        <li class="time-select__item" data-time='23:00'>23:00</li>
                    </ul>
                </div>

                <div class="time-select__group">
                    <div class="col-sm-4">
                        <p class="time-select__place">Curzon</p>
                    </div>
                    <ul class="col-sm-8 items-wrap">
                        <li class="time-select__item" data-time='09:00'>09:00</li>
                        <li class="time-select__item" data-time='11:00'>11:00</li>
                        <li class="time-select__item" data-time='13:00'>13:00</li>
                        <li class="time-select__item" data-time='15:00'>15:00</li>
                        <li class="time-select__item" data-time='17:00'>17:00</li>
                        <li class="time-select__item" data-time='19:0'>19:00</li>
                        <li class="time-select__item" data-time='21:0'>21:00</li>
                        <li class="time-select__item" data-time='23:0'>23:00</li>
                        <li class="time-select__item" data-time='01:0'>01:00</li>
                    </ul>
                </div>

                <div class="time-select__group">
                    <div class="col-sm-4">
                        <p class="time-select__place">Odeon</p>
                    </div>
                    <ul class="col-sm-8 items-wrap">
                        <li class="time-select__item" data-time='10:45'>10:45</li>
                        <li class="time-select__item" data-time='16:00'>16:00</li>
                        <li class="time-select__item" data-time='19:00'>19:00</li>
                        <li class="time-select__item" data-time='21:15'>21:15</li>
                        <li class="time-select__item" data-time='23:00'>23:00</li>
                    </ul>
                </div>

                <div class="time-select__group group--last">
                    <div class="col-sm-4">
                        <p class="time-select__place">Picturehouse</p>
                    </div>
                    <ul class="col-sm-8 items-wrap">
                        <li class="time-select__item" data-time='17:45'>17:45</li>
                        <li class="time-select__item" data-time='21:30'>21:30</li>
                        <li class="time-select__item" data-time='02:20'>02:20</li>
                    </ul>
                </div>
            </div> -->
            <!-- end time table-->

        </div>
        @endforeach
        <!-- end movie preview item -->
        <div class="coloum-wrapper">
            <div class="pagination paginatioon--full">
                <a href='#' class="pagination__prev">prev</a>
                <a href='#' class="pagination__next">next</a>
            </div>
        </div>

    </div>
    <div class="modal fade" id="trailer" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                
                    <iframe width="570" height="315" src="{{$film->film_trailer}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                
                </div>

            </div>

        </div>
    </div>
</section>
@endsection