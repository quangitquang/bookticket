@extends('welcome')
@section('content')

<!-- Search bar -->
<div class="search-wrapper">
    <div class="container container--add">
        <form id='search-form' method='get' class="search" style="margin-top: 20px;">
            <input type="text" class="search__field" placeholder="Search">
            <select name="sorting_item" id="search-sort" class="search__sort" tabindex="0">
                <option value="1" selected='selected'>By title</option>
                <option value="2">By year</option>
                <option value="3">By producer</option>
                <option value="4">By title</option>
                <option value="5">By year</option>
            </select>
            <button type='submit' class="btn btn-md btn--danger search__button">search a movie</button>
        </form>
    </div>
</div>
<!-- login -->

<section class="container ">
    <div class="panel panel-default" style="margin-top: 20px;">

        <?php

        use Illuminate\Support\Facades\Session;

        $message = Session::get('message');
        // if ($message) {
        //     echo '<div class="alert alert-danger alert-dismissible fade in">';
        //     echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
        //     echo '<span class="text-success">' . $message . '</span>';
        //     echo '</div>';
        //     Session::put('message', null);
        // }
        ?>
        <div class="login">
            @if ($message)
            <div class="alert alert-danger alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                {{$message}}
            </div>
            @endif
            <p class="login__title">Đăng Nhập</p>
        </div>
        <div class="panel-body">
            <form id="login-form" action="{{URL::to('/login-customer')}}" method="post">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="input-10" name="email" placeholder="ĐỊA CHỈ MAIL">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Mật Khẩu</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="input-10" name="password" placeholder="MẬT KHẨU">
                    </div>
                </div>
                <div class="login__control form-footer">
                    <button type="submit" class="btn btn-md btn--warning btn-block">Đăng Nhập</button>
                </div>
            </form>
        </div>
    </div>
</section>


<!-- register -->


<section class="container">
    <div class="panel panel-default">
        <div class="login">
            <p class="login__title">Đăng Ký</p>
        </div>
        <div class="panel-body">
            <form id="login-form" action="{{URL::to('/add_customer')}}" method="post">
                {{csrf_field()}}

                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Họ và tên</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="input-10" name="name_account" placeholder="HỌ VÀ TÊN">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Địa Chỉ mail</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="input-10" name="email_account" placeholder="ĐỊA CHỈ MAIL">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Số điện thoại</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="input-10" name="phone_account" placeholder="SỐ ĐIỆN THOẠI">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Mật Khẩu</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="input-10" name="password_account" placeholder="MẬT KHẨU">
                    </div>
                </div>
                <div class="login__control form-footer">
                    <button type="submit" class="btn btn-md btn--warning btn-block"><i class="fa fa-check-square-o"></i>Đăng Ký</button>
                </div>
            </form>
        </div>
    </div>
</section>
<div class="clearfix"></div>
</div>
@endsection