@extends('welcome')
@section('content')
<div class="search-wrapper">
    <div class="container container--add">
        <form id='search-form' method='get' class="search">
            <input type="text" class="search__field" placeholder="Search">
            <select name="sorting_item" id="search-sort" class="search__sort" tabindex="0">
                <option value="1" selected='selected'>By title</option>
                <option value="2">By year</option>
                <option value="3">By producer</option>
                <option value="4">By title</option>
                <option value="5">By year</option>
            </select>
            <button type='submit' class="btn btn-md btn--danger search__button">search a movie</button>
        </form>
    </div>
</div>

<section class="container">
    @foreach($film_detail as $key => $detail_film)
    <form action="" method="post">
    <div class="col-sm-12">
        <div class="movie">
            <h2 class="page-heading">{{$detail_film->film_name}}</h2>

            <div class="movie__info">
                <div class="col-sm-4 col-md-3 movie-mobile">
                    <div class="movie__images">
                        <span class="movie__rating">5.0</span>
                        <img alt='' src="{{asset('upload/films/'.$detail_film->film_image)}}">
                    </div>

                </div>
            </div>

            <div class="col-sm-8 col-md-9">
                <p class="movie__time">{{$detail_film->film_time}}</p>

                <p class="movie__option"><strong>Quốc Gia: </strong><a href="#">{{$detail_film->film_country}}</p>

                <p class="movie__option"><strong>Thể Loại: </strong><a href="#">{{$detail_film->category_name}}</a></p>
                <p class="movie__option"><strong>Ngày Khởi Chiếu: </strong>{{$detail_film->film_date}}</p>
                <p class="movie__option"><strong>Đạo diễn: </strong><a href="#">{{$detail_film->film_director}}</a></p>
                <p class="movie__option"><strong>Actors: </strong><a href="#">{{$detail_film->film_actors}},...</p>
                <p class="movie__option"><strong>Age restriction: </strong><a href="#">13</a></p>
                <p class="movie__option"><strong>Box office: </strong><a href="#">$1 017 003 568</a></p>

                <a href="#" class="comment-link">Comments: 15</a>

                <div class="movie__btns movie__btns--full">
                    <a href="" class="btn btn-md btn-danger" data-toggle="modal" data-target="#trailer">Trailer</a>
                    <a href="{{URL::to('/book-ticket/'.$detail_film->film_id)}}" class="watchlist btn--warning">Đặt Vé Bộ Phim Này</a>
                </div>

                <div class="share">
                    <span class="share__marker">Share: </span>
                    <div class="addthis_toolbox addthis_default_style ">
                        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                        <a class="addthis_button_tweet"></a>
                        <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <h2 class="page-heading">Nội Dung</h2>

        <p class="movie__describe">{{$detail_film->film_content}}</p>

    </div>
    </form>
    @endforeach
    <h2 class="page-heading">showtime &amp; tickets</h2>
    <div class="choose-container">
        <form id='select' class="select" method='get'>
            <select name="select_item" id="select-sort" class="select__sort" tabindex="0">
                <option value="1" selected='selected'>London</option>
                <option value="2">New York</option>
                <option value="3">Paris</option>
                <option value="4">Berlin</option>
                <option value="5">Moscow</option>
                <option value="3">Minsk</option>
                <option value="4">Warsawa</option>
                <option value="5">Kiev</option>
            </select>
        </form>

        <div class="datepicker">
            <span class="datepicker__marker"><i class="fa fa-calendar"></i>Date</span>
            <input type="text" id="datepicker" value='03/10/2014' class="datepicker__input">
        </div>

        <a href="#" id="map-switch" class="watchlist watchlist--map watchlist--map-full"><span class="show-map">Show cinemas on map</span><span class="show-time">Show cinema time table</span></a>

        <div class="clearfix"></div>

        <div class="time-select">
            <div class="time-select__group group--first">
                <div class="col-sm-4">
                    <p class="time-select__place">Cineworld</p>
                </div>
                <ul class="col-sm-8 items-wrap">
                    <li class="time-select__item" data-time='09:40'>09:40</li>
                    <li class="time-select__item" data-time='13:45'>13:45</li>
                    <li class="time-select__item active" data-time='15:45'>15:45</li>
                    <li class="time-select__item" data-time='19:50'>19:50</li>
                    <li class="time-select__item" data-time='21:50'>21:50</li>
                </ul>
            </div>

            <div class="time-select__group">
                <div class="col-sm-4">
                    <p class="time-select__place">Empire</p>
                </div>
                <ul class="col-sm-8 items-wrap">
                    <li class="time-select__item" data-time='10:45'>10:45</li>
                    <li class="time-select__item" data-time='16:00'>16:00</li>
                    <li class="time-select__item" data-time='19:00'>19:00</li>
                    <li class="time-select__item" data-time='21:15'>21:15</li>
                    <li class="time-select__item" data-time='23:00'>23:00</li>
                </ul>
            </div>

            <div class="time-select__group">
                <div class="col-sm-4">
                    <p class="time-select__place">Curzon</p>
                </div>
                <ul class="col-sm-8 items-wrap">
                    <li class="time-select__item" data-time='09:00'>09:00</li>
                    <li class="time-select__item" data-time='11:00'>11:00</li>
                    <li class="time-select__item" data-time='13:00'>13:00</li>
                    <li class="time-select__item" data-time='15:00'>15:00</li>
                    <li class="time-select__item" data-time='17:00'>17:00</li>
                    <li class="time-select__item" data-time='19:0'>19:00</li>
                    <li class="time-select__item" data-time='21:0'>21:00</li>
                    <li class="time-select__item" data-time='23:0'>23:00</li>
                    <li class="time-select__item" data-time='01:0'>01:00</li>
                </ul>
            </div>

            <div class="time-select__group">
                <div class="col-sm-4">
                    <p class="time-select__place">Odeon</p>
                </div>
                <ul class="col-sm-8 items-wrap">
                    <li class="time-select__item" data-time='10:45'>10:45</li>
                    <li class="time-select__item" data-time='16:00'>16:00</li>
                    <li class="time-select__item" data-time='19:00'>19:00</li>
                    <li class="time-select__item" data-time='21:15'>21:15</li>
                    <li class="time-select__item" data-time='23:00'>23:00</li>
                </ul>
            </div>

            <div class="time-select__group group--last">
                <div class="col-sm-4">
                    <p class="time-select__place">Picturehouse</p>
                </div>
                <ul class="col-sm-8 items-wrap">
                    <li class="time-select__item" data-time='17:45'>17:45</li>
                    <li class="time-select__item" data-time='21:30'>21:30</li>
                    <li class="time-select__item" data-time='02:20'>02:20</li>
                </ul>
            </div>
        </div>

        <!-- hiden maps with multiple locator-->
        <div class="map">
            <div id='cimenas-map'></div>
        </div>

        <h2 class="page-heading">comments (15)</h2>

        <div class="comment-wrapper">
            <form id="comment-form" class="comment-form" method='post'>
                <textarea class="comment-form__text" placeholder='Add you comment here'></textarea>
                <label class="comment-form__info">250 characters left</label>
                <button type='submit' class="btn btn-md btn--danger comment-form__btn">add comment</button>
            </form>

            <div class="comment-sets">

                <div class="comment">
                    <div class="comment__images">
                        <img alt='' src="http://placehold.it/50x50">
                    </div>

                    <a href='#' class="comment__author"><span class="social-used fa fa-facebook"></span>Roberta Inetti</a>
                    <p class="comment__date">today | 03:04</p>
                    <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                    <a href='#' class="comment__reply">Reply</a>
                </div>

                <div class="comment">
                    <div class="comment__images">
                        <img alt='' src="http://placehold.it/50x50">
                    </div>

                    <a href='#' class="comment__author"><span class="social-used fa fa-vk"></span>Olia Gozha</a>
                    <p class="comment__date">22.10.2013 | 14:40</p>
                    <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                    <a href='#' class="comment__reply">Reply</a>
                </div>

                <div class="comment comment--answer">
                    <div class="comment__images">
                        <img alt='' src="http://placehold.it/50x50">
                    </div>

                    <a href='#' class="comment__author"><span class="social-used fa fa-vk"></span>Dmitriy Pustovalov</a>
                    <p class="comment__date">today | 10:19</p>
                    <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                    <a href='#' class="comment__reply">Reply</a>
                </div>

                <div class="comment comment--last">
                    <div class="comment__images">
                        <img alt='' src="http://placehold.it/50x50">
                    </div>

                    <a href='#' class="comment__author"><span class="social-used fa fa-facebook"></span>Sia Andrews</a>
                    <p class="comment__date"> 22.10.2013 | 12:31 </p>
                    <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                    <a href='#' class="comment__reply">Reply</a>
                </div>

                <div id='hide-comments' class="hide-comments">
                    <div class="comment">
                        <div class="comment__images">
                            <img alt='' src="http://placehold.it/50x50">
                        </div>

                        <a href='#' class="comment__author"><span class="social-used fa fa-facebook"></span>Roberta Inetti</a>
                        <p class="comment__date">today | 03:04</p>
                        <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                        <a href='#' class="comment__reply">Reply</a>
                    </div>

                    <div class="comment">
                        <div class="comment__images">
                            <img alt='' src="http://placehold.it/50x50">
                        </div>

                        <a href='#' class="comment__author"><span class="social-used fa fa-vk"></span>Olia Gozha</a>
                        <p class="comment__date">22.10.2013 | 14:40</p>
                        <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                        <a href='#' class="comment__reply">Reply</a>
                    </div>
                </div>

                <div class="comment-more">
                    <a href="#" class="watchlist">Show more comments</a>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="trailer" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                @foreach($film_detail as $key => $detail_film)
                    <iframe width="570" height="315" src="{{$detail_film->film_trailer}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                @endforeach
                </div>

            </div>

        </div>
    </div>

</section>
@endsection