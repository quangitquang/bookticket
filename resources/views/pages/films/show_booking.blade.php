@extends('welcome')
@section('content')
<div class="search-wrapper">
    <div class="container container--add">
        <form id='search-form' method='get' class="search">
            <input type="text" class="search__field" placeholder="Search">
            <select name="sorting_item" id="search-sort" class="search__sort" tabindex="0">
                <option value="1" selected='selected'>By title</option>
                <option value="2">By year</option>
                <option value="3">By producer</option>
                <option value="4">By title</option>
                <option value="5">By year</option>
            </select>
            <button type='submit' class="btn btn-md btn--danger search__button">search a movie</button>
        </form>
    </div>
</div>

<!-- Main content -->

<section class="container">
    <div class="order-container">
        <div class="order">
            <img class="order__images" alt='' src="{{asset('frontend/images/tickets.png')}}">
            <p class="order__title">Đặt vé <br><span class="order__descript">and have fun movie time</span></p>
        </div>
    </div>
    <div class="choose-film">
        <div class="panel panel-default">
            <div class="panel-body">
                <!-- <form action="{{URL::to('/book-now')}}" method="post"> -->
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Tên Phim</th>
                                <th>loại vé</th>
                                <th>số lượng</th>
                                <th>Giá</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $all_booking as $key => $booking)

                            <tr>
                                <td>{{$booking->film_name}}</td>
                                <td>{{$booking->ticket_name}}</td>
                                <td>{{$booking->quantity}}</td>
                                <td class="choosen-place">{{number_format(((float)$booking->quantity)*($booking->ticket_money)).'đ'}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?php
                    $sum = 0;
                    ?>
                    @foreach ( $all_booking as $key => $booking)
                    <?php

                    $sum += (((float)$booking->quantity) * ($booking->ticket_money));
                    ?>
                    @endforeach
                    <?php
                    echo '<span class="text-danger" name="total">Tổng tiền:' . number_format($sum) . 'đ</span>';
                    ?>
                </div>
                <div class="col-md-2 modify--bottom float-right">
                    <div class="btn-demo">
                        <?php

                        use Illuminate\Support\Facades\Session;

                        $customer_id = Session::get('customer_id');
                        if ($customer_id == NULL) {
                        ?>
                            <a href="{{URL::to('/login-checkout')}}" class="btn btn-md btn--shine">Thanh Toán</a>
                        <?php
                        } else {
                        ?>
                            <a href="{{URL::to('/checkout')}}" class="btn btn-md btn--shine">Thanh Toán</a>
                        <?php
                        }
                        ?>
                        
                        <!-- <button type="submit" class="btn btn-md btn--shine text-right">Thanh Toán</button> -->
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection