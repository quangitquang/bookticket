@extends('welcome')
@section('content')
<section class="container">
    <div class="order-container">
        <div class="order">
            <img class="order__images" alt='' src="images/tickets.png">
            <p class="order__title">Thank you <br><span class="order__descript">Chúc bạn xem phim vui vẻ</span></p>
        </div>

        <div class="ticket">
            @foreach ( $all_booking as $key => $booking)
            <div class="ticket-position">
                <div class="ticket__indecator indecator--pre">
                    <div class="indecator-text pre--text">online ticket</div>
                </div>

                <div class="ticket__inner">

                    <div class="ticket-secondary">
                        <span class="ticket__item">Ticket number <strong class="ticket__number">a126bym4</strong></span>
                        <span class="ticket__item ticket__date">{{$booking->date}}</span>
                        <span class="ticket__item ticket__time">{{$booking->Time}}</span>
                        <span class="ticket__item">Cinema: <span class="ticket__cinema">Cineworld</span></span>
                        <span class="ticket__item">Hall: <span class="ticket__hall">Visconti</span></span>
                        <span class="ticket__item ticket__price">price: <strong class="ticket__cost">{{number_format($booking->total).'đ'}}</strong></span>
                    </div>

                    <div class="ticket-primery">
                        <span class="ticket__item ticket__item--primery ticket__film">Film<br><strong class="ticket__movie">{{$booking->filmname}}</strong></span>
                        <span class="ticket__item ticket__item--primery">Sits: <span class="ticket__place">11F, 12F, 13F</span></span>
                    </div>


                </div>

                <div class="ticket__indecator indecator--post">
                    <div class="indecator-text post--text">online ticket</div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="ticket-control">
            <a href="#" class="watchlist list--download">Download</a>
            <a href="#" class="watchlist list--print">Print</a>
        </div>

    </div>
</section>
@endsection