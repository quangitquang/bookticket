<script src="{{asset('client/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('client/assets/js/popper.min.js')}}"></script>
<script src="{{asset('client/assets/js/bootstrap.min.js')}}"></script>

<!-- simplebar js -->
<script src="{{asset('client/assets/plugins/simplebar/js/simplebar.js')}}"></script>
<!-- waves effect js -->
<script src="{{asset('client/assets/js/waves.js')}}"></script>
<!-- sidebar-menu js -->
<script src="{{asset('client/assets/js/sidebar-menu.js')}}"></script>
<!-- Custom scripts -->
<script src="{{asset('client/assets/js/app-script.js')}}"></script>
<!-- Chart js -->
<script src="{{asset('client/assets/plugins/Chart.js/Chart.min.js')}}"></script>
<!--Peity Chart -->
<script src="{{asset('client/assets/plugins/peity/jquery.peity.min.js')}}"></script>
<!-- Index js -->
<script src="{{asset('client/assets/js/index.js')}}"></script>
<!--Bootstrap Datepicker Js-->
<script src="{{asset('client/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
    $('#default-datepicker').datepicker({
        todayHighlight: true
    });
    $('#autoclose-datepicker').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    $('#inline-datepicker').datepicker({
        todayHighlight: true
    });

    $('#dateragne-picker .input-daterange').datepicker({});
</script>

<!--Multi Select Js-->
<script src="{{asset('client/assets/plugins/jquery-multi-select/jquery.multi-select.js')}}"></script>
<script src="{{asset('client/assets/plugins/jquery-multi-select/jquery.quicksearch.js')}}"></script>

<script>
    $(document).ready(function() {
        $('.single-select').select2();

        $('.multiple-select').select2();

        //multiselect start

        $('#my_multi_select1').multiSelect();
        $('#my_multi_select2').multiSelect({
            selectableOptgroup: true
        });

        $('#my_multi_select3').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function(ms) {
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e) {
                        if (e.which == 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function() {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function() {
                this.qs1.cache();
                this.qs2.cache();
            }
        });

        $('.custom-header').multiSelect({
            selectableHeader: "<div class='custom-header'>Selectable items</div>",
            selectionHeader: "<div class='custom-header'>Selection items</div>",
            selectableFooter: "<div class='custom-header'>Selectable footer</div>",
            selectionFooter: "<div class='custom-header'>Selection footer</div>"
        });



    });
</script>

<!--material date picker js-->
<script src="{{asset('client/assets/plugins/material-datepicker/js/moment.min.js')}}"></script>
<script src="{{asset('client/assets/plugins/material-datepicker/js/bootstrap-material-datetimepicker.min.js')}}"></script>
<script src="{{asset('client/assets/plugins/material-datepicker/js/ja.js')}}"></script> 

<script>
    $(function() {

        // dat time picker
        $('#date-time-picker').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm'
        });

        // only date picker
        $('#date-picker').bootstrapMaterialDatePicker({
            time: false
        });

        // only time picker
        $('#time-picker').bootstrapMaterialDatePicker({
            date: false,
            format: 'HH:mm'
        });


    });
</script>