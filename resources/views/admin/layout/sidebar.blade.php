<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
    <div class="brand-logo">
        <a href="{{URL::to('/admin/dashboard')}}">
            
            <h5 class="logo-text">Hello Admin</h5>
        </a>
    </div>
    <ul class="sidebar-menu do-nicescrol">
        
        <li>
            <a href="{{URL::to('/admin/dashboard')}}" class="waves-effect">
                <i class="zmdi zmdi-view-dashboard"></i> <span>Tổng Quan</span>
            </a>
        </li>

        <li>
            <a href="javaScript:void();" class="waves-effect">
                <i class="zmdi zmdi-format-list-bulleted"></i> <span>Quản Lý Phim</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{URL::to('/add-film')}}"><i class="zmdi zmdi-star-outline"></i> Thêm Mới</a></li>
                <li><a href="{{URL::to('/all-film')}}"><i class="zmdi zmdi-star-outline"></i> Danh Sách</a></li>
            </ul>
        </li>
        <li>
            <a href="javaScript:void();" class="waves-effect">
                <i class="zmdi zmdi-format-list-bulleted"></i> <span>Quản Lý Thể Loại</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{route('addCategory')}}"><i class="zmdi zmdi-star-outline"></i> Thêm Mới</a></li>
                <li><a href="{{route('AllCatergory')}}"><i class="zmdi zmdi-star-outline"></i> Danh Sách</a></li>
            </ul>
        </li>
        @hasrole('superadmin')
        <li>
            <a href="javaScript:void();" class="waves-effect">
                <i class="zmdi zmdi-accounts"></i> <span>Quản Lý User</span>
                <i class="fa fa-angle-left float-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{URL::to('/add-user')}}"><i class="zmdi zmdi-star-outline"></i> Thêm mới</a></li>
                <li><a href="{{URL('all-user')}}"><i class="zmdi zmdi-star-outline"></i> Danh Sách</a></li>
                
            </ul>
        </li>
        @endhasrole
        <li>
            <a href="javaScript:void();" class="waves-effect">
                <i class="zmdi zmdi-format-list-bulleted"></i> <span>Quản Lý Vé</span>
                <i class="fa fa-angle-left float-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{URL::to('/add-ticket')}}"><i class="zmdi zmdi-star-outline"></i> Thêm Mới</a></li>
                <li><a href="{{URL::to('/all-ticket')}}"><i class="zmdi zmdi-star-outline"></i> Danh Sách</a></li>
                
            </ul>
        </li>

        <li>
            <a href="javaScript:void();" class="waves-effect">
                <i class="zmdi zmdi-format-list-bulleted"></i> <span>Quản Lý Rạp</span>
                <i class="fa fa-angle-left float-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{URL::to('/add-cinema')}}"><i class="zmdi zmdi-star-outline"></i> Thêm Mới</a></li>
                <li><a href="{{URL::to('/all-cinema')}}"><i class="zmdi zmdi-star-outline"></i> Danh Sách</a></li>
            </ul>
        </li>

    </ul>

</div>