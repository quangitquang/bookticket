@extends('admin.layout.admin_layout')
@section('admin_content')
<!-- danh sách phim -->
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title text-center">Danh sách Thể Loại</h4>
      </div>
      <?php

      use Illuminate\Support\Facades\Session;

      $message = Session::get('message');
      if ($message) {
        echo '<span class="text-center text-success">' . $message . '</span>';
        Session::put('message', null);
      }
      ?>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class=" text-primary">
              <th>Loại Vé</th>
              <th>Giá vé</th>
            </thead>
            <tbody>
              @foreach($all_tickets as $key => $ticket_pro)
              <tr>

                <td>{{$ticket_pro->ticket_name}}</td>
                <td>{{$ticket_pro->ticket_money}}</td>
                <td class="text-primary">
                  <a href="{{URL::to('/edit-ticket/'.$ticket_pro->tickets_id)}}"><i class="fa fa-pencil-square-o text-success"></i></a>
                  <a onclick="return confirm('Bạn Có Muốn Xóa!!!')" href="{{URL::to('/delete-ticket/'.$ticket_pro->tickets_id)}}"><i class="fa fa-times text-danger"></i></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection