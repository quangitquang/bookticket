@extends('admin.layout.admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <?php

                use Illuminate\Support\Facades\Session;

                $message = Session::get('message');
                if ($message) {
                    echo '<span class="text-success">' . $message . '</span>';
                    Session::put('message', null);
                }
                ?>
                @foreach($edit_users as $key => $edit_value)
                <form id="signupForm" action="{{URL::to('/update-user/'.$edit_value->id)}}" method="post">
                    {{csrf_field()}}
                    <h4 class="form-header text-uppercase">
                        Cập Nhật user
                    </h4>

                    <div class="form-group row">
                        <label for="input-16" class="col-sm-2 col-form-label">Tên Người Dùng</label>
                        <div class="col-sm-10">
                            <input name="name" type="text" class="form-control" id="input-16" value="{{$edit_value->name}}">
                        </div>
                        
                    </div>
                    <div class="form-group row">
                        <label for="input-16" class="col-sm-2 col-form-label">Mật Khẩu Mới</label>
                        <div class="col-sm-10">
                            <input name="password" type="password" class="form-control" id="input-16" value="{{$edit_value->password}}">
                        </div>
                    </div>

                    <div class="form-group row">
                    <label for="input-17" class="col-sm-2 col-form-label">Role</label>
                        <div class="col-sm-10">
                            <select class="form-control selectpicker"  id="input-6" name="roles[]" data-style="select-with-transition">
                                
                                @foreach($role as $item)
                                <option selected value="{{$item->id}}">{{$item->display_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-footer">
                        <!-- <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button> -->
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Cập Nhật</button>
                    </div>

                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection