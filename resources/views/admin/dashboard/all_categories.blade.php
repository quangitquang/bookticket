@extends('admin.layout.admin_layout')
@section('admin_content')
<!-- danh sách phim -->
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title text-center">Danh sách Thể Loại</h4>
      </div>
      <?php

      use Illuminate\Support\Facades\Session;

      $message = Session::get('message');
      if ($message) {
        echo '<span class="text-center text-success">' . $message . '</span>';
        Session::put('message', null);
      }
      ?>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class=" text-primary">
              <th>Thể Loại</th>
              <th>Mô Tả</th>
            </thead>
            <tbody>
              @foreach($all_categories as $key => $cate_pro)
              <tr>

                <td>{{$cate_pro->category_name}}</td>
                <td>{{$cate_pro->category_status}}</td>
                <td class="text-primary">
                  <a href="{{URL::to('/edit-category/'.$cate_pro->category_id)}}"><i class="fa fa-pencil-square-o text-success"></i></a>
                  <a onclick="return confirm('Bạn Có Muốn Xóa!!!')" href="{{URL::to('/delete-category/'.$cate_pro->category_id)}}"><i class="fa fa-times text-danger"></i></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection