@extends('admin.layout.admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <?php

                use Illuminate\Support\Facades\Session;

                $message = Session::get('message');
                if ($message) {
                    echo '<span class="text-success">' . $message . '</span>';
                    Session::put('message', null);
                }
                ?>
                @foreach($edit_films as $key => $pro)
                <form id="signupForm" action="{{URL::to('/update-film/'.$pro->film_id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}                   
                    <h4 class="form-header text-uppercase">
                        Cập Nhật Phim
                    </h4>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-2 col-form-label">Tên Film</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-10" name="film_name" value="{{$pro->film_name}}">
                        </div>
                        <label for="input-11" class="col-sm-2 col-form-label">Thời gian</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-11" name="film_time" value="{{$pro->film_time}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-11" class="col-sm-2 col-form-label">Ngày Khởi Chiếu</label>
                        <div class="col-sm-4">
                            <input type="text" id="autoclose-datepicker" class="form-control" name="film_date" value="{{$pro->film_date}}">
                        </div>
                        <label for="input-6" class="col-sm-2 col-form-label">Thể Loại</label>
                        <div class="col-sm-4">
                            <select class="form-control" id="input-6" name="film_cate" required>
                                @foreach($cate_categories as $key => $cate_pro)
                                    @if($cate_pro->category_id==$pro->category_id)
                                    <option selected value="{{$cate_pro->category_id}}">{{$cate_pro->category_name}}</option>
                                    @else
                                    <option value="{{$cate_pro->category_id}}">{{$cate_pro->category_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-2 col-form-label">Tác Giả</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-10" name="film_director" value="{{$pro->film_director}}">
                        </div>
                        <label for="input-11" class="col-sm-2 col-form-label">Quốc gia</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-11" name="film_country" value="{{$pro->film_country}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-2 col-form-label">Trailer phim</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-10" name="film_trailer" value="{{$pro->film_trailer}}">
                        </div>
                        <label for="input-11" class="col-sm-2 col-form-label">Diễn Viên</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-11" name="film_actor" value="{{$pro->film_actors}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-8" class="col-sm-2 col-form-label">Chọn Ảnh</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" id="input-8" name="film_image" required>
                            <img src="{{URL::to('upload/films/'.$pro->film_image)}}" height="100" width="100">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-17" class="col-sm-2 col-form-label">Nội Dung</label>
                        <div class="col-sm-10">
                            <textarea name="film_content" style="resize:none;" class="form-control" rows="4" id="input-17">{{$pro->film_content}}</textarea>
                        </div>
                    </div>
                    <div class="form-footer">
                        <!-- <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button> -->
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Cập Nhật</button>
                    </div>
                    
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection