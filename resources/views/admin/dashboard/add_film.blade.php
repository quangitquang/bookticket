@extends('admin.layout.admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="signupForm" action="{{URL::to('/save-film')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <h4 class="form-header text-uppercase">
                        Thêm Mới Phim
                    </h4>
                    <?php

                    use Illuminate\Support\Facades\Session;

                    $message = Session::get('message');
                    if ($message) {
                        echo '<span class="text-success">' . $message . '</span>';
                        Session::put('message', null);
                    }
                    ?>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-2 col-form-label">Tên Film</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-10" name="film_name">
                        </div>
                        <label for="input-11" class="col-sm-2 col-form-label">Thời gian</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-11" name="film_time">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-11" class="col-sm-2 col-form-label">Ngày Khởi Chiếu</label>
                        <div class="col-sm-4">
                            <input type="text" id="autoclose-datepicker" class="form-control" name="film_date">
                        </div>
                        <label for="input-6" class="col-sm-2 col-form-label">Thể Loại</label>
                        <div class="col-sm-4">
                            <select class="form-control" id="input-6" name="film_cate" multiple>
                            <!-- <select class="form-control" id="input-6" name="film_cate" required multiple size="3"> -->
                            @foreach($cate_categories as $key => $cate_pro)
                                <option value="{{$cate_pro->category_id}}">{{$cate_pro->category_name}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-2 col-form-label">Tác Giả</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-10" name="film_director">
                        </div>
                        <label for="input-11" class="col-sm-2 col-form-label">Quốc gia</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-11" name="film_country">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-2 col-form-label">Trailer phim</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-10" name="film_trailer">
                        </div>
                        <label for="input-11" class="col-sm-2 col-form-label">Diễn Viên</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-11" name="film_actor">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-8" class="col-sm-2 col-form-label">Chọn Ảnh</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" id="input-8" name="film_image" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-17" class="col-sm-2 col-form-label">Nội Dung</label>
                        <div class="col-sm-10">
                            <textarea name="film_content" style="resize:none;" class="form-control" rows="4" id="input-17"></textarea>
                        </div>
                    </div>
                    <div class="form-footer">
                        <!-- <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button> -->
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Thêm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection