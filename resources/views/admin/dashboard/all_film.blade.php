@extends('admin.layout.admin_layout')
@section('admin_content')
<!-- danh sách phim -->
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title text-center">Danh sách phim</h4>
      </div>
      <?php

      use Illuminate\Support\Facades\Session;

      $message = Session::get('message');
      if ($message) {
        echo '<span class="text-center text-success">' . $message . '</span>';
        Session::put('message', null);
      }
      ?>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead class=" text-primary">
              <th>STT</th>
              <th>Tên phim</th>
              <th>Thời Gian</th>
              <th>Thể Loại</th>
              <th>Ngày Khởi Chiếu</th>
              <th>Hình Ảnh</th>
              <th>Nội Dung</th>
            </thead>
            <tbody>
              @foreach($all_films as $key => $film_pro)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$film_pro->film_name}}</td>
                <td>{{$film_pro->film_time}}</td>
                <td>{{$film_pro->category_name}}</td>
                <td>{{$film_pro->film_date}}</td>
                <td><img src="{{asset('upload/films/'.$film_pro->film_image)}}" height="50" width="50"></td>
                <td>{{$film_pro->film_content}}</td>
                <td class="text-primary">
                  <a href="{{URL::to('/edit-film/'.$film_pro->film_id)}}"><i class="fa fa-pencil-square-o text-success"></i></a>
                  <a onclick="return confirm('Bạn Có Muốn Xóa!!!')" href="{{URL::to('/delete-film/'.$film_pro->film_id)}}"><i class="fa fa-times text-danger"></i></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div> 
@endsection