<?php

use Illuminate\Support\Facades\Session; ?>

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/rukada/color-admin/authentication-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 09 Nov 2019 15:12:17 GMT -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Đăng Nhập</title>
    @include('admin.layout.style')

</head>

<body class="bg-dark">
    <!-- Start wrapper-->
    <div id="wrapper">
        <div class="card card-authentication1 mx-auto my-5">
            <div class="card-body">
                <div class="card-content p-2">

                    <div class="card-title text-uppercase text-center py-3">Admin</div>
                    <form action="{{ route('login') }}" method="post">
                        {{csrf_field()}}
                        <?php

                        $message = Session::get('message');
                        if ($message) {
                            echo '<span class="text-danger">' . $message . '</span>';
                            Session::put('message', null);
                        }
                        ?>
                        <div class="form-group">
                            <label for="exampleInputUsername" class="">Tên Đăng Nhập</label>
                            <div class="position-relative has-icon-right">
                                <input name="email" type="email" id="email" class="form-control input-shadow" value="{{ old('email') }}" placeholder="Nhập Email" required autocomplete="email">
                                <div class="form-control-position">
                                    <i class="icon-user"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword" class="">Mật Khẩu</label>
                            <div class="position-relative has-icon-right">
                                <input id="password" type="password" class="form-control input-shadow" name="password" placeholder="Nhập Mật Khẩu" required autocomplete="current-password">
                                <div class="form-control-position">
                                    <i class="icon-lock"></i>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary shadow-primary btn-block waves-effect waves-light">Đăng Nhập</button>
                    </form>
                    
                </div>
            </div>

        </div>

        <!--Start Back To Top Button-->
        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
        <!--End Back To Top Button-->
    </div>
    <!--wrapper-->

    <!-- Bootstrap core JavaScript-->
    @include('admin.layout.javascript')

</body>

<!-- Mirrored from codervent.com/rukada/color-admin/authentication-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 09 Nov 2019 15:12:17 GMT -->

</html>