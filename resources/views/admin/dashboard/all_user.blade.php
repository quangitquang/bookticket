@extends('admin.layout.admin_layout')
@section('admin_content')
<!-- danh sách phim -->
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title text-center">Danh sách User</h4>
      </div>
      <?php

      use Illuminate\Support\Facades\Session;

      $message = Session::get('message');
      if ($message) {
        echo '<span class="text-center text-success">' . $message . '</span>';
        Session::put('message', null);
      }
      ?>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class=" text-primary">
              <th>STT</th>
              <th>Tên</th>
              <th>Số điện Thoại</th>
              <th>Role</th>

            </thead>
            <tbody>
              @foreach($users as $user)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$user->name}}</td>

                <td>@if($user->roles->count() >0)
                  @foreach($user->roles as $role)
                  {{$role->display_name .','}}
                  @endforeach
                  @endif
                </td>
                <td class="text-primary">
                  <a href="{{URL::to('/edit-user/'.$user->id)}}"><i class="fa fa-pencil-square-o text-success"></i></a>
                  <a onclick="return confirm('Bạn Có Muốn Xóa!!!')" href="{{URL::to('/delete-user/'.$user->id)}}"><i class="fa fa-times text-danger"></i></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection