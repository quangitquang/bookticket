@extends('admin.layout.admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <?php

                use Illuminate\Support\Facades\Session;

                $message = Session::get('message');
                if ($message) {
                    echo '<span class="text-success">' . $message . '</span>';
                    Session::put('message', null);
                }
                ?>
                @foreach($edit_categories as $key => $edit_value)
                <form id="signupForm" action="{{URL::to('/update-categories/'.$edit_value->category_id)}}" method="post">
                    {{csrf_field()}}
                    <h4 class="form-header text-uppercase">
                        Cập Nhật Thể Loại
                    </h4>

                    <div class="form-group row">
                        <label for="input-16" class="col-sm-2 col-form-label">Tên Thể Loại</label>
                        <div class="col-sm-10">
                            <input value="{{$edit_value->category_name}}" name="categories_name" type="text" class="form-control" id="input-16" name="contactnumber">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input-17" class="col-sm-2 col-form-label">Mô Tả</label>
                        <div class="col-sm-10">
                            <textarea value="text" name="categories_status" style="resize:none;" class="form-control" rows="4" id="input-17">{{$edit_value->category_status}}</textarea>
                        </div>
                    </div>
                    <div class="form-footer">
                        <!-- <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button> -->
                        <button type="submit" name="update_categories" class="btn btn-success"><i class="fa fa-check-square-o"></i> Cập Nhật</button>
                    </div>

                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection