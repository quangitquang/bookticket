@extends('admin.layout.admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <?php

                use Illuminate\Support\Facades\Session;

                $message = Session::get('message');
                if ($message) {
                    echo '<span class="text-success">' . $message . '</span>';
                    Session::put('message', null);
                }
                ?>
                @foreach($edit_ticket as $key => $ticket_edit)
                <form id="signupForm" action="{{URL::to('/update-ticket/'.$ticket_edit->tickets_id)}}" method="post">
                    {{csrf_field()}}
                    <h4 class="form-header text-uppercase">
                        Cập Nhật Vé
                    </h4>

                    <div class="form-group row">
                        <label for="input-16" class="col-sm-2 col-form-label">Tên Loại Vé</label>
                        <div class="col-sm-10">
                            <input value="{{$ticket_edit->ticket_name}}" name="ticket_name" type="text" class="form-control" id="input-16" name="contactnumber">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input-17" class="col-sm-2 col-form-label">Giá</label>
                        <div class="col-sm-10">
                            <input value="{{$ticket_edit->ticket_money}}" name="ticket_money" class="form-control" rows="4" id="input-16">
                        </div>
                    </div>
                    <div class="form-footer">
                        <!-- <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button> -->
                        <button type="submit" name="update_categories" class="btn btn-success"><i class="fa fa-check-square-o"></i> Cập Nhật</button>
                    </div>

                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection