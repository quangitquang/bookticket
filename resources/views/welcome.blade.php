<!doctype html>
<html>

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>QCinema</title>
    <meta name="description" content="A Template by Gozha.net">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Gozha.net">

    <!-- Mobile Specific Metas-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="telephone=no" name="format-detection">

    @include('pages.style')
</head>

<body>
    <div class="wrapper">
        <!-- Banner -->
        <div class="banner-top">
            <img alt='top banner' src="{{asset('frontend/images/banners/bra.jpg')}}">
        </div>

        <!-- Header section -->
        @include('pages.header')

        <!-- Slider -->
        @include('pages.slider')

        <!--end slider -->


        <!-- Main content -->
        @yield('content')

        <div class="clearfix"></div>

        <footer class="footer-wrapper">
            <section class="container">
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="#" class="nav-link__item">Cities</a></li>
                        <li><a href="movie-list-left.html" class="nav-link__item">Movies</a></li>
                        <li><a href="trailer.html" class="nav-link__item">Trailers</a></li>
                        <li><a href="rates-left.html" class="nav-link__item">Rates</a></li>
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="coming-soon.html" class="nav-link__item">Coming soon</a></li>
                        <li><a href="cinema-list.html" class="nav-link__item">Cinemas</a></li>
                        <li><a href="offers.html" class="nav-link__item">Best offers</a></li>
                        <li><a href="news-left.html" class="nav-link__item">News</a></li>
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="#" class="nav-link__item">Terms of use</a></li>
                        <li><a href="gallery-four.html" class="nav-link__item">Gallery</a></li>
                        <li><a href="contact.html" class="nav-link__item">Contacts</a></li>
                        <li><a href="page-elements.html" class="nav-link__item">Shortcodes</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="footer-info">
                        <p class="heading-special--small">A.Movie<br><span class="title-edition">in the social media</span></p>

                        <div class="social">
                            <a href='#' class="social__variant fa fa-facebook"></a>
                            <a href='#' class="social__variant fa fa-twitter"></a>
                            <a href='#' class="social__variant fa fa-vk"></a>
                            <a href='#' class="social__variant fa fa-instagram"></a>
                            <a href='#' class="social__variant fa fa-tumblr"></a>
                            <a href='#' class="social__variant fa fa-pinterest"></a>
                        </div>

                        <div class="clearfix"></div>
                        <p class="copy">&copy; A.Movie, 2013. All rights reserved. Done by Olia Gozha</p>
                    </div>
                </div>
            </section>
        </footer>
    </div>

    <!-- open/close -->
    @include('pages.signin')

    <!-- JavaScript-->
    <!-- jQuery 1.9.1-->
    <script src="{{asset('frontend/js/external/jquery-1.10.1.min.js')}}"></script>
    <script>
        window.jQuery || document.write('<script src="{{asset('frontend/js/external/jquery-1.10.1.min.js')}}"><\/script>')
    </script>
    <!-- Migrate -->
    <script src="{{asset('frontend/js/external/jquery-migrate-1.2.1.min.js')}}"></script>
    <!-- Bootstrap 3-->
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

    <!-- jQuery REVOLUTION Slider -->
    <script type="text/javascript" src="{{asset('frontend/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>

    <!-- Mobile menu -->
    <script src="{{asset('frontend/js/jquery.mobile.menu.js')}}"></script>
    <!-- Select -->
    <script src="{{asset('frontend/js/external/jquery.selectbox-0.2.min.js')}}"></script>
    <!-- Stars rate -->
    <script src="{{asset('frontend/js/external/jquery.raty.js')}}"></script>

    <!-- Form element -->
    <script src="{{asset('frontend/js/external/form-element.js')}}"></script>
    <!-- Form validation -->
    <script src="{{asset('frontend/js/form.js')}}"></script>

    <!-- Twitter feed -->
    <script src="{{asset('frontend/js/external/twitterfeed.js')}}"></script>

    <!-- Custom -->
    <script src="{{asset('frontend/js/custom.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            init_Home();
        });
    </script>

</body>

</html>
