<?php

use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//FRONT-END
Route::get('/', 'Page\TrangChuController@index');
//DANH MỤC PHIM
Route::get('/danh-muc-phim/{category_id}', 'FilmsController@show_category');
Route::get('/film-detail/{film_id}', 'FilmsController@details_film');
//đặt phim
Route::get('/book-ticket/{film_id}', 'CartController@book');
Route::post('/next-booking', 'BookingController@save_booking');
Route::get('/booking', 'BookingController@show');
Route::get('/login-checkout', 'CheckOutController@login_chekout');
Route::get('/logout-checkout', 'CheckOutController@logout_chekout');
Route::get('/checkout', 'CheckOutController@checkout');
Route::post('/add_customer', 'CheckOutController@add_customer');
Route::post('/login-customer', 'CheckOutController@login_customer');
Route::get('/show-ticket/{id}', 'CheckOutController@show');

//admin
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'UserController@index')->name('admin');
    Route::get('/dashboard', 'UserController@show_dashboard');
});

//user
Route::middleware(['auth', 'role:superadmin'])->group(function () {
    Route::get('/add-user', 'AuthController@register_auth');
    Route::post('/save-user', 'UserController@add');
    Route::get('/login-auth', 'AuthController@login_auth');
    Route::get('/all-user', 'UserController@show');
    Route::get('/edit-user/{id}', 'UserController@edit');
    Route::post('/update-user/{id}', 'UserController@update');
    Route::get('/delete-user/{id}', 'UserController@destroy');
});

//

//categories
Route::group(['prefix' => 'Category', 'middleware' => 'auth',], function () {
    Route::get('/add-category', 'CategoriesController@create')->name('addCategory');
    Route::get('/all-category', 'CategoriesController@show')->name('AllCatergory');
});
Route::group(['middleware' => 'auth',], function () {
    //categories
    Route::get('/edit-category/{category_id}', 'CategoriesController@edit');
    Route::get('/delete-category/{category_id}', 'CategoriesController@destroy');
    Route::post('/save-category', 'CategoriesController@store');
    Route::post('/update-categories/{category_id}', 'CategoriesController@update');
});


//film
Route::get('/add-film', 'FilmsController@create');
Route::get('/all-film', 'FilmsController@show');
Route::get('/edit-film/{film_id}', 'FilmsController@edit');
Route::get('/delete-film/{film_id}', 'FilmsController@destroy');
Route::post('/save-film', 'FilmsController@store');
Route::post('/update-film/{film_id}', 'FilmsController@update');


//ticket
Route::get('/add-ticket', 'TicketController@create');
Route::get('/all-ticket', 'TicketController@show');
Route::get('/edit-ticket/{tickets_id}', 'TicketController@edit');
Route::get('/delete-ticket/{tickets_id}', 'TicketController@destroy');
Route::post('/save-ticket', 'TicketController@store');
Route::post('/update-ticket/{tickets_id}', 'TicketController@update');


//cinema
Route::get('/add-cinema', 'CinemaController@create');
Route::get('/all-cinema', 'CinemaController@show');
Route::get('/edit-cinema/{cinema_id}', 'CinemaController@edit');
Route::get('/delete-cinema/{cinema_id}', 'CinemaController@destroy');
Route::post('/save-cinema', 'CinemaController@store');
Route::post('/update-cinema/{cinema_id}', 'CinemaController@update');

//Route::post('/update-film/{film_id}', 'FilmsController@update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
