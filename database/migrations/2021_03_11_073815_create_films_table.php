<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('film_id');
            $table->integer('category_id');
            $table->string('film_name');
            $table->string('film_country');
            $table->string('film_director');
            $table->string('film_actors');
            $table->string('film_trailer');
            $table->string('film_time');
            $table->string('film_date');
            $table->string('film_image');
            $table->text('film_content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
