<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create role
        $rolesuperAdmin = Role::create(['name' => 'superadmin', 'display_name' => 'Quản lý chung']);
        $roleAdmin = Role::create(['name' => 'admin', 'display_name' => 'Quản lý']);
        $roleeditor = Role::create(['name' => 'editor', 'display_name' => 'chỉnh sửa']);
        $roleuser = Role::create(['name' => 'user', 'display_name' => 'người dùng']);
        // create permission list array

        // $permission = Permission::create(['name' => 'admin-user-film-category', 'display_name' => 'super admin']);

        // $rolesuperAdmin->givePermissionTo($permission);
        // $permission->assignRole($rolesuperAdmin);

        $managefilm = Permission::create(['name'=>'film-manage', 'display_name'=>'quan ly phim']);
        $manageAdmin = Permission::create(['name'=>'user-manage', 'display_name'=>'quan nguoi dung']);
        $manageAdmin = Permission::create(['name'=>'category-manage', 'display_name'=>'quan ly the loai']);
        Permission::create(['name'=>'guest', 'display_name'=>'xem pages']);

        $roleuser->givePermissionTo('guest');
        $roleAdmin->givePermissionTo($manageAdmin->name);
        $roleAdmin->givePermissionTo($managefilm->name);
        $roleeditor->givePermissionTo($managefilm->name);
        

        $userAdmin = User::where('email', 'quang@gmail.com')->first();
        if (is_null($userAdmin)) {
            $user = new User();
            $user->name = "nguyen van quang";
            $user->email = "quang@gmail.com";
            // $user->admin_phone = "0373813614";
            $user->password = Hash::make('12345');
            $user->save();
        }

        if(!$userAdmin->hasRole('superadmin')){
            $userAdmin ->assignRole('superadmin');
        }

    }
}
