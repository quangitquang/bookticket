<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userAdmin = User::where('email', 'quang@gmail.com')->first();
        if (is_null($userAdmin)) {
            $user = new User();
            $user->name = "nguyen van quang";
            $user->email = "quang@gmail.com";
            // $user->admin_phone = "0373813614";
            $user->password = Hash::make('12345678');
            $user->save();
        }
    }
}
